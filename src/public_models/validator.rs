use crate::public_models::domain::{
    egress_ip_range::EgressIpRange, encryption_pub_key::EncryptionPubKey,
    validator_auth_address::ValidatorAuthAddress,
    validator_finalize_grandpa_key::ValidatorFinalizeGrandpaKey,
    validator_produce_aura_key::ValidatorProduceAuraKey,
};

#[derive(Clone, Debug)]
pub struct ValidatorIdentity {
    pub key_gen_summary: ValidatorKeys,
    pub cidr_blocks: Vec<EgressIpRange>,
}

#[derive(Clone, Default, Debug)]
pub struct ValidatorKeys {
    pub val_kp_pub: ValidatorAuthAddress,
    pub produce_blocks_kp_pub: ValidatorProduceAuraKey,
    pub finalize_blocks_kp_pub: ValidatorFinalizeGrandpaKey,
    pub pub_key: EncryptionPubKey,
}
