use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct EncryptionPubKey(pub(crate) [u8; 32]);

impl Serialize for EncryptionPubKey {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let bytes = self.0;
        let serialized_b58 = bs58::encode(bytes).into_string();
        serializer.serialize_str(serialized_b58.as_str())
    }
}

impl<'de> Deserialize<'de> for EncryptionPubKey {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let mut buf: [u8; 32] = [0u8; 32];
        bs58::decode(s).into(&mut buf).map_err(|err| {
            de::Error::custom(format!("Could not decode encryption key {:?}", err))
        })?;
        Ok(EncryptionPubKey(buf))
    }
}

impl From<[u8; 32]> for EncryptionPubKey {
    fn from(bytes: [u8; 32]) -> Self {
        Self(bytes)
    }
}
