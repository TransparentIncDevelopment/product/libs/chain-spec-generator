use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::str::FromStr;

// TODO: replace with xand_address?
#[derive(Clone, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct Address(pub(crate) String);

impl FromStr for Address {
    type Err = ();

    fn from_str(addr: &str) -> Result<Self, Self::Err> {
        // TODO: Add validation and perhaps detailed error
        //
        // Leaving as-is for now because the interface is
        // correct. Keep tabs on our canonical string encodings page
        // and efforts to consolidate serialization concerns per
        // domain type so we don't have to repeat error-prone code.
        // https://transparentinc.atlassian.net/wiki/spaces/PROD/pages/1306624009/Domain+Types+and+Canonical+String+Encoding+Serialization
        Ok(Self(addr.to_owned()))
    }
}

impl Display for Address {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}
