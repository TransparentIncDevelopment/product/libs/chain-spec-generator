/// 1 Day in milliseconds, more than enough time for the trust to verify
pub(crate) const PENDING_CREATE_EXPIRE_TIME: u64 = 86_400_000;
/// Roughly 2 days in milliseconds because ids must outlive create requests
pub(crate) const USED_CORRELATION_ID_EXPIRE_TIME: u64 = 1.73e8 as u64;
