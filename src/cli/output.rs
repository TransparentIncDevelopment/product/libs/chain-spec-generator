use std::fmt::Display;

use super::commands::GenerateOutput;

pub enum CliOutput {
    Generate(GenerateOutput),
}

impl Display for CliOutput {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            CliOutput::Generate(output) => output.fmt(f),
        }
    }
}

impl From<GenerateOutput> for CliOutput {
    fn from(out: GenerateOutput) -> Self {
        CliOutput::Generate(out)
    }
}
