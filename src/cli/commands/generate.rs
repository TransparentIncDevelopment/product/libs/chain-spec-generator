use std::{fmt::Display, fs::File, path::PathBuf};

use config::ConfigurationFile;
pub use error::GenerateError;

use crate::cli::options::GenerateCommandArgs;

pub(crate) mod config;
mod error;

pub struct GenerateOutput {
    pub chainspec_path: PathBuf,
}

impl Display for GenerateOutput {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Successfully generated chainspec: {:?}",
            self.chainspec_path
        )
    }
}

pub fn run_generate_command(args: GenerateCommandArgs) -> Result<GenerateOutput, GenerateError> {
    let file = File::open(&args.config_file).map_err(|e| {
        GenerateError::FileIo(
            format!("Error while opening config file {:?}", args.config_file),
            e,
        )
    })?;
    let config: ConfigurationFile = serde_yaml::from_reader(file).map_err(|e| {
        GenerateError::SerdeParse(
            format!("Error while parsing config file {:?}", args.config_file),
            e,
        )
    })?;

    let chainspec = config.build_chain_spec_data()?;

    std::fs::create_dir_all(&args.output_dir).map_err(|e| {
        GenerateError::FileIo(
            format!(
                "Error while creating target directory {:?}",
                args.output_dir
            ),
            e,
        )
    })?;
    let chainspec_path = chainspec.generate_into(args.chainspec_zip, args.output_dir)?;

    Ok(GenerateOutput { chainspec_path })
}
