use thiserror::Error;

use crate::public_models::domain::egress_ip_range::EgressIpRangeParseError;

#[derive(Debug, Error)]
pub enum GenerateError {
    #[error("{}", .0)]
    AddressParse(#[from] EgressIpRangeParseError),
    #[error("{}: {}", .0, .1)]
    FileIo(String, std::io::Error),
    #[error("{}: {}", .0, .1)]
    SerdeParse(String, serde_yaml::Error),
    #[error("{}", .0)]
    ChainspecGeneration(#[from] crate::Error),
}
