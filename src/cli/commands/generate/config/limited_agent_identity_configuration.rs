use serde_derive::Deserialize;

use crate::{cli::commands::GenerateError, public_models::domain::address::Address};

use super::parse_cidr_block_list;

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct LimitedAgentIdentityConfiguration {
    pub address: Address,

    pub ip_address_ranges: Vec<String>,
}

impl LimitedAgentIdentityConfiguration {
    pub fn build_limited_agent_identity(
        &self,
    ) -> Result<crate::LimitedAgentIdentity, GenerateError> {
        Ok(crate::LimitedAgentIdentity {
            cidr_blocks: parse_cidr_block_list(&self.ip_address_ranges)?,
            address: self.address.clone(),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::LimitedAgentIdentityConfiguration;
    use crate::{cli::GenerateError::AddressParse, public_models::domain::address::Address};

    use assert_matches::assert_matches;

    #[test]
    fn build_limited_agent_identity__can_parse_valid_ip() {
        // Given
        let config: LimitedAgentIdentityConfiguration = LimitedAgentIdentityConfiguration {
            address: Address("someaddr".into()),
            ip_address_ranges: vec!["127.0.0.1".into()],
        };

        // When
        let ident = config.build_limited_agent_identity().unwrap();

        // Then
        assert_eq!(ident.address, Address("someaddr".into()));
        assert_eq!(ident.cidr_blocks, vec!["127.0.0.1".parse().unwrap()]);
    }

    #[test]
    fn build_limited_agent_identity__errors_on_invalid_ip() {
        // Given
        let config: LimitedAgentIdentityConfiguration = LimitedAgentIdentityConfiguration {
            address: Address("someaddr".into()),
            ip_address_ranges: vec!["notARealIpAddress".into()],
        };

        // When
        let ident = config.build_limited_agent_identity();

        // Then
        assert_matches!(ident.err().unwrap(), AddressParse(..));
    }

    #[test]
    fn from_str__can_parse_yaml_str() {
        let source_str = r#"
        address: 5CADH2EYpHkLhTy7t8F4WMv749oGuqp8L3LfsiHiAT1PzXDr
        ip_address_ranges: [ "127.0.0.1" ]
        "#;

        let config: LimitedAgentIdentityConfiguration = serde_yaml::from_str(source_str).unwrap();

        assert_eq!(
            config.address.0,
            "5CADH2EYpHkLhTy7t8F4WMv749oGuqp8L3LfsiHiAT1PzXDr"
        );
        assert_eq!(config.ip_address_ranges[..], ["127.0.0.1"]);
    }
}
