use crate::{
    cli::commands::GenerateError,
    public_models::domain::{
        encryption_pub_key::EncryptionPubKey, validator_auth_address::ValidatorAuthAddress,
    },
};

use serde_derive::Deserialize;

use super::parse_cidr_block_list;

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct ValidatorIdentityConfiguration {
    pub address: ValidatorAuthAddress,
    pub aura_session_key: String,
    pub grandpa_session_key: String,
    pub encryption_pub_key: EncryptionPubKey,

    pub ip_address_ranges: Vec<String>,
}

impl ValidatorIdentityConfiguration {
    pub fn build_validator_identity(&self) -> Result<crate::ValidatorIdentity, GenerateError> {
        Ok(crate::ValidatorIdentity {
            cidr_blocks: parse_cidr_block_list(&self.ip_address_ranges)?,
            key_gen_summary: crate::ValidatorKeys {
                val_kp_pub: self.address.clone(),
                produce_blocks_kp_pub: self.aura_session_key.clone().into(),
                finalize_blocks_kp_pub: self.grandpa_session_key.clone().into(),
                pub_key: self.encryption_pub_key.clone(),
            },
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::cli::commands::GenerateError;

    use super::ValidatorIdentityConfiguration;

    use crate::public_models::domain::encryption_pub_key::EncryptionPubKey;
    use assert_matches::assert_matches;

    #[test]
    fn build_validator_identity__can_parse_valid_ip() {
        // Given
        let config = ValidatorIdentityConfiguration {
            address: "someaddr".into(),
            ip_address_ranges: vec!["127.0.0.1".into()],
            aura_session_key: "aura_key".into(),
            grandpa_session_key: "grandpa_key".into(),
            encryption_pub_key: EncryptionPubKey([5; 32]),
        };

        // When
        let ident = config.build_validator_identity().unwrap();

        // Then
        assert_eq!(ident.key_gen_summary.val_kp_pub, "someaddr".into());
        assert_eq!(ident.cidr_blocks, vec!["127.0.0.1".parse().unwrap()]);
        assert_eq!(
            ident.key_gen_summary.produce_blocks_kp_pub,
            "aura_key".into()
        );
        assert_eq!(
            ident.key_gen_summary.finalize_blocks_kp_pub,
            "grandpa_key".into()
        );
    }

    #[test]
    fn build_validator_identity__errors_on_invalid_ip() {
        // Given
        let config = ValidatorIdentityConfiguration {
            address: "someaddr".into(),
            aura_session_key: "aura_key".into(),
            grandpa_session_key: "grandpa_key".into(),
            ip_address_ranges: vec!["notARealIpAddress".into()],
            encryption_pub_key: EncryptionPubKey([5; 32]),
        };

        // When
        let ident = config.build_validator_identity();

        // Then
        assert_matches!(ident.err().unwrap(), GenerateError::AddressParse(..))
    }

    #[test]
    fn from_str__can_parse_yaml_str() {
        let source_str = r#"
        address: 5Dd9WpDQcuHbzHeV4STUbNZKGiaEWRs58KtWjTWUcdDaq2LJ
        aura_session_key: 5Dd9WpDQcuHbzHeV4STUbNZKGiaEWRs58KtWjTWUcdDaq2LJ
        grandpa_session_key: 5F7Qp9rgETxE2ZLaJqNPT2b55kDUSMkQSAzd25Mr6PL5yVfV
        encryption_pub_key: Nc3hoeZaZmiSJcXmmYN1i5p65LBXscpHLFFRJJKhZfE
        ip_address_ranges: [ "127.0.0.1" ]
        "#;

        let config: ValidatorIdentityConfiguration = serde_yaml::from_str(source_str).unwrap();

        assert_eq!(
            config.address.to_string(),
            "5Dd9WpDQcuHbzHeV4STUbNZKGiaEWRs58KtWjTWUcdDaq2LJ"
        );
        assert_eq!(
            config.aura_session_key,
            "5Dd9WpDQcuHbzHeV4STUbNZKGiaEWRs58KtWjTWUcdDaq2LJ"
        );
        assert_eq!(
            config.grandpa_session_key,
            "5F7Qp9rgETxE2ZLaJqNPT2b55kDUSMkQSAzd25Mr6PL5yVfV"
        );
        assert_eq!(
            config.encryption_pub_key.0,
            bs58::decode("Nc3hoeZaZmiSJcXmmYN1i5p65LBXscpHLFFRJJKhZfE")
                .into_vec()
                .unwrap()[..]
        );
        assert_eq!(config.ip_address_ranges[..], ["127.0.0.1"]);
    }
}
