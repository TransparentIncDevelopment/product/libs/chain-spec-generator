use serde_derive::Deserialize;

use crate::{
    cli::commands::GenerateError,
    public_models::domain::{address::Address, encryption_pub_key::EncryptionPubKey},
};

use super::parse_cidr_block_list;

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct MemberIdentityConfiguration {
    pub address: Address,
    pub encryption_pub_key: EncryptionPubKey,

    pub ip_address_ranges: Vec<String>,
}

impl MemberIdentityConfiguration {
    pub fn build_member_identity(&self) -> Result<crate::MemberIdentity, GenerateError> {
        Ok(crate::MemberIdentity {
            cidr_blocks: parse_cidr_block_list(&self.ip_address_ranges)?,
            address: self.address.clone(),
            pub_key: self.encryption_pub_key.clone(),
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::cli::GenerateError;
    use crate::public_models::domain::{address::Address, encryption_pub_key::EncryptionPubKey};

    use assert_matches::assert_matches;

    use super::MemberIdentityConfiguration;

    #[test]
    fn build_member_identity__can_parse_valid_ip() {
        // Given
        let config = MemberIdentityConfiguration {
            address: Address("someaddr".into()),
            ip_address_ranges: vec!["127.0.0.1".into()],
            encryption_pub_key: EncryptionPubKey([5; 32]),
        };

        // When
        let ident = config.build_member_identity().unwrap();

        // Then
        assert_eq!(ident.address, Address("someaddr".into()));
        assert_eq!(ident.cidr_blocks, vec!["127.0.0.1".parse().unwrap()]);
        assert_eq!(ident.pub_key, EncryptionPubKey([5; 32]));
    }

    #[test]
    fn build_member_identity__errors_on_invalid_ip() {
        // Given
        let config = MemberIdentityConfiguration {
            address: Address("someaddr".into()),
            ip_address_ranges: vec!["notARealIpAddress".into()],
            encryption_pub_key: EncryptionPubKey([5; 32]),
        };

        // When
        let ident = config.build_member_identity();

        // Then
        assert_matches!(ident.err().unwrap(), GenerateError::AddressParse(..))
    }

    #[test]
    fn from_str__can_parse_yaml_str() {
        let source_str = r#"
        address: 5Gph9iMER7s492Rm7i51oJkCr5snibMVWV6oqfEXEuxhv5xM
        encryption_pub_key: FWA6qr2sSwgYYjvkrnNbBJjzXoCU1QLiq3ibiEYuEBVm
        ip_address_ranges: [ "127.0.0.1" ]
        "#;

        let config: MemberIdentityConfiguration = serde_yaml::from_str(source_str).unwrap();

        assert_eq!(
            config.address.0,
            "5Gph9iMER7s492Rm7i51oJkCr5snibMVWV6oqfEXEuxhv5xM"
        );
        assert_eq!(
            config.encryption_pub_key.0,
            bs58::decode("FWA6qr2sSwgYYjvkrnNbBJjzXoCU1QLiq3ibiEYuEBVm")
                .into_vec()
                .unwrap()[..]
        );
        assert_eq!(config.ip_address_ranges[..], ["127.0.0.1"]);
    }
}
