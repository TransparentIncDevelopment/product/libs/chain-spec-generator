#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![allow(non_snake_case)]
#[cfg(test)]
#[macro_use]
extern crate mockall;

mod error;
mod output;

mod generation;

pub use generation::ChainSpec;

mod public_models;
pub use public_models::{
    limited_agent::LimitedAgentIdentity,
    member::MemberIdentity,
    trust::TrustIdentity,
    validator::{ValidatorIdentity, ValidatorKeys},
    validator_emissions::EmissionRate,
};

pub use error::Error;
pub type Result<T, E = Error> = std::result::Result<T, E>;

// CLI interface
pub mod cli;
